function thanhToan() {
  var soThuNhat = document.getElementById("txt-so-thu-nhat").value * 1;
  var soThuHai = document.getElementById("txt-so-thu-hai").value * 1;
  var soThuBa = document.getElementById("txt-so-thu-ba").value * 1;
  if (soThuNhat < soThuHai && soThuNhat < soThuBa) {
    if (soThuHai < soThuBa) {
      document.getElementById(
        "result"
      ).innerHTML = `${soThuNhat} , ${soThuHai} , ${soThuBa}`;
    } else {
      document.getElementById(
        "result"
      ).innerHTML = `${soThuNhat} , ${soThuBa} , ${soThuHai}`;
    }
  } else if (soThuHai < soThuNhat && soThuHai < soThuBa) {
    if (soThuNhat < soThuBa) {
      document.getElementById(
        "result"
      ).innerHTML = `${soThuHai} , ${soThuNhat} , ${soThuBa}`;
    } else {
      document.getElementById(
        "result"
      ).innerHTML = `${soThuHai} , ${soThuBa} , ${soThuNhat}`;
    }
  } else if (soThuBa < soThuNhat && soThuBa < soThuHai) {
    if (soThuNhat < soThuHai) {
      document.getElementById(
        "result"
      ).innerHTML = `${soThuBa} , ${soThuNhat} , ${soThuHai}`;
    } else {
      document.getElementById(
        "result"
      ).innerHTML = `${soThuBa} , ${soThuHai} , ${soThuNhat}`;
    }
  }
}
function thanhToan2() {
  var bo, me, anhTrai, emGai;

  if (document.getElementById("sel1").value == 1) {
    document.getElementById("result2").innerHTML = `Chào Bố!`;
  } else if (document.getElementById("sel1").value == 2) {
    document.getElementById("result2").innerHTML = `Chào Mẹ!`;
  } else if (document.getElementById("sel1").value == 3) {
    document.getElementById("result2").innerHTML = `Chao Anh trai!`;
  } else if (document.getElementById("sel1").value == 4) {
    document.getElementById("result2").innerHTML = `Chao Em gái!`;
  }
  if (document.getElementById("sel1").value == 0) {
    document.getElementById("result2").innerHTML = `Chưa chọn thành viên !`;
  }
}

function thanhToan3() {
  var so1,
    so2,
    so3,
    soChan = 0,
    soLe = 0,
    count = 0;

  so1 = document.getElementById("txt-so1").value * 1;
  so2 = document.getElementById("txt-so2").value * 1;
  so3 = document.getElementById("txt-so3").value * 1;

  if (so1 % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }

  if (so2 % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }

  if (so3 % 2 == 0) {
    soChan++;
  } else {
    soLe++;
  }

  document.getElementById("result3").innerHTML = `Có ${soChan} số chẳn`;
  document.getElementById("result4").innerHTML = `Có ${soLe} lẻ`;
}
function thanhToan4() {
  var a = document.getElementById("txt-canhA").value * 1;
  var b = document.getElementById("txt-canhB").value * 1;
  var c = document.getElementById("txt-canhC").value * 1;

  if (a + b > c && a + c > b && b + c > a) {
    if (a == b && a == c && b == c) {
      document.getElementById("result5").innerHTML = `La tam giac deu`;
    } else if (a == b || a == c || b == c) {
      document.getElementById("result5").innerHTML = `La tam giac can`;
    } else if (
      Math.pow(a, 2) == Math.pow(b, 2) + Math.pow(c, 2) ||
      Math.pow(b, 2) == Math.pow(a, 2) + Math.pow(c, 2) ||
      Math.pow(c, 2) == Math.pow(a, 2) + Math.pow(b, 2)
    ) {
      document.getElementById("result5").innerHTML = `La tam giac vuong`;
    } else {
      document.getElementById("result5").innerHTML = `La tam giac khac`;
    }
  } else {
    document.getElementById(
      "result5"
    ).innerHTML = `Nhập sai ! Tổng 2 cạnh này phải lớn hơn cạnh còn lại,  nhập lại!`;
  }
}
